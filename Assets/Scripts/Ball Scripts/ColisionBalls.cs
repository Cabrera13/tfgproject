﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisionBalls : MonoBehaviour
{
    private Vector3 movementCurrentDirection;
    // Start is called before the first frame update
Vector3 reflectionDirection;
    private Vector3 lastVelocity;
    private Vector3 goalVelocity;
    public Rigidbody myRigidbody;
    void Start()
    {

        myRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {   

    }


    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag != "wall"){
            movementCurrentDirection = transform.TransformDirection(1,1,0);
            reflectionDirection = Vector3.Reflect( movementCurrentDirection, other.transform.position);
        }
    }
}
