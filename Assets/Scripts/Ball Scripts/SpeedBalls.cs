﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBalls : MonoBehaviour
{
    float constantSpeed;
    float t; 
    // Start is called before the first frame update
    void Start()
    {
        constantSpeed = 10.0f;
    }

    // Update is called once per frame
    void Update()
    {
	    GetComponent<Rigidbody>().velocity = constantSpeed * (GetComponent<Rigidbody>().velocity.normalized);
        t+= Time.deltaTime; 
        if (t >=2.0f) {
            transform.position = new Vector3(transform.position.x, 0.36f, transform.position.z);
        }
    }
}
