﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBalls : MonoBehaviour
{
    float t = 0.0f;
    public GameObject[] d; 

    // Start is called before the first frame update
    void Start()
    {
        //Instantiate(d[Random.Range(0,3)], d[Random.Range(0,3)].transform);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        t+= Time.deltaTime;
        if (t >= 3.0f) {
            var j = d[Random.Range(0,3)];
            Instantiate(j,j.transform);
            t = 0.0f;

        }

    }
}
