﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    public float speed;
    private Rigidbody rb;
	// Use this for initialization
	void Start () {
		 rb = GetComponent<Rigidbody>();
		 transform.position = new Vector3(0.50f, 0.0f, -9.20f);
	}
	// Update is called once per frame
	void Update () {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, 0.0f);
		rb.velocity = movement * speed;
	}
}
