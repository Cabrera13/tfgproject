﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI : MonoBehaviour {
	public bool isVerticalPlayer = false;
	GameObject closest = null;
	public float rightLimit = 7.50f; 
	public float leftLimit = -6.50f;
	public float zPositiveLimit = 5.15f;
	public float zNegativeLimit = -5.50f;

	void setup () {

	}

	void Update ()  {
		GameObject[] spheres = GameObject.FindGameObjectsWithTag("ball");
		Vector3 position = transform.position;
		float distance = Mathf.Infinity;
		foreach (GameObject go in spheres) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		if (!isVerticalPlayer){
				if (closest.transform.position.x > transform.position.x && transform.position.x < rightLimit) {
					transform.position = new Vector3 (transform.position.x + 0.10f, transform.position.y, transform.position.z);
				} else if (closest.transform.position.x < transform.position.x && transform.position.x > leftLimit) {
					transform.position = new Vector3 (transform.position.x - 0.10f, transform.position.y, transform.position.z);
				}
		}
		else {
				if (closest.transform.position.z > transform.position.z && transform.position.z < zPositiveLimit) {
					transform.position = new Vector3 (transform.position.x , transform.position.y, transform.position.z+ 0.10f);
				} else if (closest.transform.position.z < transform.position.z && transform.position.z > zNegativeLimit) {
					transform.position = new Vector3 (transform.position.x , transform.position.y, transform.position.z- 0.10f);
				}
		}
		}

}